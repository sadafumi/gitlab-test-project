#include <iostream>
#include <random>

int main() 
{
	std::random_device rng;
	for (int i = 0; i < 100; i++)
	{
		unsigned int result = rng();
		std::cout  << i << " " << result << std::endl;
	}
	getchar();
}